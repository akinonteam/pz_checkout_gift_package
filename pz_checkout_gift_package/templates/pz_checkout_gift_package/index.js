import { store } from 'shop-packages';
import observe, { getValue } from 'shop-packages/redux/connectSelector';
import { selectGiftable, selectGiftNote } from 'shop-packages/redux/checkout/selectors';
import { setGiftBox, deleteGiftBox } from 'shop-packages/redux/checkout/actions';
import { replaceTag } from '@Utils';

export default class PzCheckoutGiftBox {
  /**
   * Settings object (destructured parameters)
   * @typedef {Object} Settings
   * @property {String} [defaultGiftMessage=NO-GIFT-MESSAGE]
   * @property {String} [activeInfoText=Gift Pack Added]
   * @property {String} [addPackageTriggerText=Add Gift Pack]
   * @property {String} [removeGiftNoteButtonText=Remove Gift Pack]
   * @property {String} [addGiftNoteTriggerText=Gift Note]
   * @property {String} [giftboxReviewUptadeNoteText=Update Note]
   * @property {String} [giftboxReviewTitleText=This order will be gift packaged*]
   * @property {String} [giftInputPlaceholder=You can leave empty this area. However it will be a gift package without note.] The placeholder of gift note input.
   * @property {String} [noteInputCharacterLeftMessage=characters left]
   * @property {String} [submitNoteButtonText=SAVE] The text of submit button.
   * @property {String} [removeNoteButtonText=Remove Gift Note] The text of remove gift note button.
   * @property {String} [giftboxContainerSelector=.js-checkout-giftbox-container] Selector of container.
   * @property {String} [giftboxTitleSelector=.js-cgb-header-title]
   * @property {String} [giftboxRemoveButtonSelector=.js-giftbox-remove-button]
   * @property {String} [giftboxContentSelector=.js-cgb-content]
   * @property {String} [giftboxReviewSelector=.js-giftbox-review]
   * @property {String} [giftboxReviewNoteSelector=.js-giftbox-review-note]
   * @property {String} [giftboxReviewUptadeNoteSelector=.js-giftbox-review-update-note]
   * @property {String} [giftboxExpandable=.js-cgb-expandable]
   * @property {String} [giftboxModalSelector=.js-cgb-modal]
   * @property {String} [noteInputSelector=.js-cgb-input] Selector of gift note input.
   * @property {String} [noteCurrentLengthSelector=.js-note-input-current] Selector current length of gift note.
   * @property {String} [noteMaxLengthSelector=.js-note-input-max] Selector max length of gift note.
   * @property {String} [noteInputCharacterLeftMessageSelector=.js-note-input-message]
   * @property {String} [noteSubmitButtonSelector=.js-cgb-note-submit-button] Selector of submit note button.
   * @property {String} [noteRemoveButtonSelector=.js-cgb-remove-note-button] Selector of remove note button.
   * @property {String} [icon=giftbox] icon.
   * @property {Number} [noteMinLength=0] Minimum length of gift note.
   * @property {Number} [noteMaxLength=160] Maximum length of gift note.
   * @property {Boolean} [useDefaultTemplates=true]
   * @property {Boolean} [useModal=false]
   * @property {String} [modalData=.cgb-modal]
   */
  /**
   * An instance of PZ Checkout Gift Package module
   * @param {Settings} settings An object containing settings for the instance
   */
  constructor(settings = {}) {
    if (!getValue(selectGiftable)) {
      console.warn('pz-checkout-giftbox package added but `has_gift_box` attribute still false.');
      return;
    }

    this.settings = {
      defaultGiftMessage: gettext('NO-GIFT-MESSAGE'),
      activeInfoText: gettext('Gift Pack Added'),
      addPackageTriggerText: gettext('Add Gift Pack'),
      removeGiftNoteButtonText: gettext('Remove Gift Pack'),
      addGiftNoteTriggerText: gettext('Gift Note'),
      giftInputPlaceholder: gettext('You can leave empty this area. However it will be a gift package without note.'),
      noteInputCharacterLeftMessage: gettext('characters left'),
      submitNoteButtonText: gettext('SAVE'),
      removeNoteButtonText: gettext('Remove Gift Note'),
      giftboxReviewUptadeNoteText: gettext('Update Note'),
      giftboxReviewTitleText: gettext('This order will be gift packaged*'),
      giftboxContainerSelector: '.js-checkout-giftbox-container',
      giftboxTitleSelector: '.js-cgb-header-title',
      giftboxRemoveButtonSelector: '.js-giftbox-remove-button',
      giftboxContentSelector: '.js-cgb-content',
      giftboxReviewSelector: '.js-giftbox-review',
      giftboxReviewNoteSelector: '.js-giftbox-review-note',
      giftboxReviewUptadeNoteSelector: '.js-giftbox-review-update-note',
      giftboxExpandableSelector: '.js-cgb-expandable',
      giftboxModalSelector: '.js-cgb-modal',
      noteInputSelector: '.js-cgb-input',
      noteCurrentLengthSelector: '.js-note-input-current',
      noteMaxLengthSelector: '.js-note-input-max',
      noteInputCharacterLeftMessageSelector: '.js-note-input-message',
      noteSubmitButtonSelector: '.js-cgb-note-submit-button',
      noteRemoveButtonSelector: '.js-cgb-remove-note-button',
      icon: 'pz-icon-giftbox',
      noteMinLength: 0,
      noteMaxLength: 160,
      useDefaultTemplates: true,
      useModal: false,
      modalData: '.cgb-modal'
    };

    this.isGiftboxActive = false;
    this.giftboxContainer = document.querySelector(this.settings.giftboxContainerSelector);
    this.countValidClass = '-valid-count';
    this.countErrorClass = '-invalid-count';

    this.bindSettings(settings);
    this.render();
    this.initElements();
    this.initGiftBoxOperations();
    this.initObservers();
  }

  bindSettings(settings) {
    Object.keys(this.settings)
      .filter((key) => Object.keys(settings).includes(key))
      .forEach((key) => {
        this.settings[key] = settings[key];
      });
  }

  initElements() {
    this.elements = {
      giftboxTitle: document.querySelector(this.settings.giftboxTitleSelector),
      giftboxContent: document.querySelector(this.settings.giftboxContentSelector),
      giftboxRemoveButton: document.querySelector(this.settings.giftboxRemoveButtonSelector),
      giftboxReview: document.querySelector(this.settings.giftboxReviewSelector),
      giftboxReviewNote: document.querySelector(this.settings.giftboxReviewNoteSelector),
      giftboxReviewUptadeNote: document.querySelector(this.settings.giftboxReviewUptadeNoteSelector),
      giftboxExpandable: document.querySelector(this.settings.giftboxExpandableSelector),
      giftboxModal: document.querySelector(this.settings.giftboxModalSelector),
      noteInput: document.querySelector(this.settings.noteInputSelector),
      noteCurrentLength: document.querySelector(this.settings.noteCurrentLengthSelector),
      noteMaxLength: document.querySelector(this.settings.noteMaxLengthSelector),
      noteInputCharacterLeftMessage: document.querySelector(this.settings.noteInputCharacterLeftMessageSelector),
      noteSubmitButton: document.querySelector(this.settings.noteSubmitButtonSelector),
      noteRemoveButton: document.querySelector(this.settings.noteRemoveButtonSelector)
    };

    this.giftNote = getValue(selectGiftNote);

    this.elements.noteRemoveButton.hidden = [null, '', this.settings.defaultGiftMessage].some((item) => item === this.giftNote);

    this.elements.giftboxReview.hidden = !(this.giftNote !== null);
    this.elements.giftboxRemoveButton.hidden = !(this.giftNote !== null);

    if (this.giftNote !== null) {
      this.elements.giftboxTitle.innerHTML = this.settings.activeInfoText;
    }

    if (this.elements.noteInput && this.giftNote !== this.settings.defaultGiftMessage) {
      this.elements.noteInput.value = this.giftNote;
    }

    if (this.elements.noteCurrentLength) {
      this.elements.noteCurrentLength.innerHTML = 0;
    }

    if (this.elements.noteMaxLength) {
      this.elements.noteMaxLength.innerHTML = this.settings.noteMaxLength;
    }
  }

  noteLengthIsValid(length) {
    return length <= this.settings.noteMaxLength && length >= this.settings.noteMinLength;
  }

  initGiftBoxOperations() {
    const checkNoteLength = (input) => {
      const length = input.value.length;
      const isValid = this.noteLengthIsValid(length);

      if (this.elements.noteCurrentLength) {
        this.elements.noteCurrentLength.innerHTML = length;
      }

      this.giftboxContainer.classList.toggle(this.countValidClass, isValid);
      this.giftboxContainer.classList.toggle(this.countErrorClass, !isValid);
      this.elements.noteSubmitButton.disabled = !isValid;
    };

    this.elements.giftboxTitle?.addEventListener('click', () => {
      if (!this.isGiftboxActive) {
        this.elements.giftboxContent.hidden = false;
        this.elements.giftboxReview.hidden = false;
        this.elements.giftboxRemoveButton.hidden = false;
        this.elements.giftboxTitle.innerHTML = this.settings.activeInfoText;

        store.dispatch(setGiftBox(this.settings.defaultGiftMessage));
        this.isGiftboxActive = true;
      }
    });

    this.elements.noteSubmitButton?.addEventListener('click', (e) => {
      let value = replaceTag(this.elements.noteInput.value);

      if (!this.noteLengthIsValid(value.length)) {
        this.giftboxContainer.classList.add(this.countErrorClass);
        e.currentTarget.disabled = true;
        return false;
      }

      if (value.length === 0) {
        store.dispatch(setGiftBox(this.settings.defaultGiftMessage));
      } else {
        store.dispatch(setGiftBox(value));
      }

      this.elements.noteRemoveButton.hidden = false;
      this.elements.giftboxContent.hidden = true;
      this.elements.giftboxReview.hidden = false;
    });

    this.elements.noteRemoveButton.addEventListener('click', (e) => {
      store.dispatch(setGiftBox(this.settings.defaultGiftMessage));
      this.elements.noteInput.value = '';
      e.currentTarget.hidden = true;
    });

    this.elements.giftboxRemoveButton.addEventListener('click', (e) => {
      store.dispatch(deleteGiftBox());
      this.isGiftboxActive = false;
      this.elements.giftboxReview.hidden = true;
      this.elements.giftboxContent.hidden = true;
      this.elements.noteInput.value = '';
      this.elements.giftboxTitle.innerHTML = this.settings.addPackageTriggerText;
      e.currentTarget.hidden = true;
    });

    this.elements.giftboxReviewUptadeNote.addEventListener('click', () => {
      this.elements.giftboxContent.hidden = false;
      this.elements.giftboxExpandable?.setAttribute('expanded', true);
      this.elements.giftboxModal?.show();
    });

    [
      'change',
      'paste',
      'keyup',
      'blur',
      'focus'
    ].forEach((event) => this.elements.noteInput?.addEventListener(event, (e) => checkNoteLength(e.target)));

    this.elements.noteInput.value.length > 0 && checkNoteLength(this.elements.noteInput);
  }

  initObservers() {
    this.observers = [observe(selectGiftNote).subscribe(() => {
      this.elements.giftboxReviewNote.innerText = getValue(selectGiftNote);

      if (this.elements.giftboxReviewNote.innerText === this.settings.defaultGiftMessage) {
        this.elements.giftboxReviewNote.innerText = getValue(selectGiftNote).replaceAll('-', ' ');
      }
    })];
  }

  render() {
    if (!this.giftboxContainer || !this.settings.useDefaultTemplates) { return; }

    const giftNoteView = () => `
      <pz-textarea
        class="cgb-input js-cgb-input"
        name="gift_note"
        v-minlength=${this.settings.noteMinLength}
        v-maxlength=${this.settings.noteMaxLength}
        placeholder="${this.settings.giftInputPlaceholder}";
        w-full
      >
      </pz-textarea>
    `;

    const contentFooterView = () => `
      <div class="content-footer">
        <div class="content-footer__counter">
          <span class="js-note-input-current">0</span>
          /
          <span class="js-note-input-max"></span>
          <span class="js-note-input-message">${this.settings.noteInputCharacterLeftMessage}</span>
        </div>

        <div class="content-footer__controls">
          <div class="content-footer__controls-remove-note-button js-cgb-remove-note-button" hidden>${this.settings.removeNoteButtonText}</div>

          <pz-button
            class="cgb-submit-button js-cgb-note-submit-button"
            size="xs"
            appearance="outlined"
          >
            ${this.settings.submitNoteButtonText}
          </pz-button>
        </div>
      </div>
    `;

    const expandableView = () => `
      <pz-expandable
        class="cgb-container__content-expandable js-cgb-expandable"
        title="${this.settings.addGiftNoteTriggerText}"
        hide-toggle
      >
        ${cgbContentView()}
      </pz-expandable>
    `;

    const cgbContentView = () => `
      <div class="content">
        ${giftNoteView()}
        ${contentFooterView()}
      </div>
    `;

    const modalView = () => `
      <pz-modal
        theme="classic"
        class="cgb-modal js-cgb-modal"
        modal-title="${this.settings.addGiftNoteTriggerText}"
        no-button
      >
        ${cgbContentView()}
      </pz-modal>
    `;

    let template = `
      <div class="cgb-container">
        <div class="cgb-container__header">
          <div class="cgb-container__header-left">
            <i class="cgb-container__header-icon js-cgb-header-icon ${this.settings.icon}"></i>

            <div class="cgb-container__header-title js-cgb-header-title">
              ${this.settings.addPackageTriggerText}
            </div>
          </div>

          <div class="cgb-container__header-right">
            <div class="cgb-container__header-remove-button js-giftbox-remove-button">
              ${this.settings.removeGiftNoteButtonText}
            </div>
          </div>
        </div>

        <div class="cgb-container__review js-giftbox-review" hidden>
          <div class="cgb-container__review-top">
            <div class="cgb-container__review-title">
              ${this.settings.giftboxReviewTitleText}
            </div>
            <div class="cgb-container__review-icon">
              <i class="pz-icon-giftbox"></i>
            </div>
          </div>

          <div class="cgb-container__review-bottom">
            <div class="cgb-container__review-gift-note js-giftbox-review-note">
              ${getValue(selectGiftNote)}
            </div>
            <div class="cgb-container__review-change-button js-giftbox-review-update-note">
              ${this.settings.giftboxReviewUptadeNoteText}
            </div>
          </div>
        </div>

        <div class="cgb-container__content js-cgb-content" hidden>
          ${this.settings.useModal ? modalView() : expandableView()}
        </div>
      </div>
    `;

    this.giftboxContainer.insertAdjacentHTML('beforeend', template);
  }
}