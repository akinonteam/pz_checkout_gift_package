# pz_checkout_gift_package
A library module to enable Gift Package for products on the checkout page.
## Usage
### Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```bash
# For latest version
-e git+https://git@bitbucket.org/akinonteam/pz_checkout_gift_package.git#egg=pz_checkout_gift_package

# For specific version (Recommended)
-e git+https://git@bitbucket.org/akinonteam/pz_checkout_gift_package.git@d7d544d#egg=pz_checkout_gift_package
```

For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```
### Install the npm package

```bash
# in /templates

# For latest version
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_checkout_gift_package.git

# For specific version (Recommended)
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_checkout_gift_package.git#d7d544d
```

Make sure to use the same git commit id as in `requirements.txt`.

### Add following line to omnife_base/settings.py:

```python
# omnife_base/settings.py:
INSTALLED_APPS.append('pz_checkout_gift_package')
```
### Inject Middlewares (For only lower than v2.0.0)

> **⚠ WARNING:** If your project version 2.0.0 or higher skip this step.

```js
// in templates/layout/index.js
import PzCheckoutGiftBox from 'pz-checkout-gift-package';

...
// at end of sheet (after all HttpClient.use(...) funcions)
PzCheckoutGiftBox.applyMiddlewares(HttpClient.middlewares);
```

### Import and initialize JS

### For classic (w/ actions and content) type:
```js
// in templates/orders/checkout/summary/index.js

import PzCheckoutGiftBox from 'pz-checkout-gift-package';

class Summary {
  ...
  constructor(...){
    ...
    new PzCheckoutGiftBox();
    ...
  }
  ...
}
```

### Add HTML Element

```html
// in templates/orders/checkout/summary/index.html

<div class="js-checkout-giftbox-container"></div>
```

### Import styles:

```scss
// in templates/orders/checkout/summary/index.scss
@import '~pz-checkout-gift-package/';
```

## JS settings

```js
new PzCheckoutGiftBox({
  noteMinLength: 0,
  noteMaxLength: 160,
  icon: 'pz-icon-giftbox',
  defaultGiftMessage: 'NO GIFT MESSAGE',
  activeInfoText: 'Gift Pack Added',
  addPackageTriggerText: 'Add Gift Pack',
  removeGiftNoteButtonText: 'Remove Gift Pack',
  addGiftNoteTriggerText: 'Gift Note',
  giftboxReviewUptadeNoteText: 'Update Note',
  giftboxReviewTitleText: 'This order will be gift packaged*',
  giftInputPlaceholder: 'You can leave empty this area. However it will be a gift package without note.',
  noteInputCharacterLeftMessage: 'characters left',
  submitNoteButtonText: 'SAVE',
  removeNoteButtonText: 'Remove Gift Note',
  giftboxContainerSelector: '.js-checkout-giftbox-container',
  giftboxTitleSelector: '.js-cgb-header-title',
  giftboxRemoveButtonSelector: '.js-giftbox-remove-button',
  giftboxContentSelector: '.js-cgb-content',
  giftboxReviewSelector: '.js-giftbox-review',
  giftboxReviewNoteSelector: '.js-giftbox-review-note',
  giftboxReviewUptadeNoteSelector: '.js-giftbox-review-update-note',
  giftboxExpandable: '.js-cgb-expandable',
  giftboxModalSelector: '.js-cgb-modal',
  noteInputSelector: '.js-cgb-input',
  noteCurrentLengthSelector: '.js-note-input-current',
  noteMaxLengthSelector: '.js-note-input-max',
  noteInputCharacterLeftMessageSelector: '.js-note-input-message',
  noteSubmitButtonSelector: '.js-cgb-note-submit-button',
  noteRemoveButtonSelector: '.js-cgb-remove-note-button',
  modalData: '.cgb-modal',
  useModal: true,
  useDefaultTemplates: true
});
```

- **noteMinLength**: (Number, *Optional*)  
- **noteMaxLength**: (Number, *Optional*)  
- **icon**: (String, *Optional*)  
- **defaultGiftMessage**: (String, *Optional*)  
- **activeInfoText**: (String, *Optional*)  
- **addPackageTriggerText**: (String, *Optional*)  
- **removeGiftNoteButtonText**: (String, *Optional*)  
- **addGiftNoteTriggerText**: (String, *Optional*)  
- **giftboxReviewUptadeNoteText**: (String, *Optional*)  
- **giftboxReviewTitleText**: (String, *Optional*)  
- **giftInputPlaceholder**: (String, *Optional*)  
- **noteInputCharacterLeftMessage**: (String, *Optional*)  
- **submitNoteButtonText**: (String, *Optional*)  
- **removeNoteButtonText**: (String, *Optional*)  
- **giftboxContainerSelector**: (String, *Optional*)  
- **giftboxTitleSelector**: (String, *Optional*)  
- **giftboxRemoveButtonSelector**: (String, *Optional*)  
- **giftboxContentSelector**: (String, *Optional*)  
- **giftboxReviewSelector**: (String, *Optional*)  
- **giftboxReviewNoteSelector**: (String, *Optional*)  
- **giftboxReviewUptadeNoteSelector**: (String, *Optional*)  
- **giftboxExpandable**: (String, *Optional*)  
- **giftboxModalSelector**: (String, *Optional*)  
- **noteInputSelector**: (String, *Optional*)  
- **noteCurrentLengthSelector**: (String, *Optional*)  
- **noteMaxLengthSelector**: (String, *Optional*)  
- **noteInputCharacterLeftMessageSelector**: (String, *Optional*)  
- **noteSubmitButtonSelector**: (String, *Optional*)  
- **noteRemoveButtonSelector**: (String, *Optional*)
- **modalData**: (String, *Optional*)
- **useModal**: (Boolean, *Optional*)  
- **useDefaultTemplates**: (Boolean, *Optional*)  